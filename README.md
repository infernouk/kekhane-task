# README #

This README documents how to run the Kekhane app

### Running for dev ###

* Requirements

Ensure you have Homebrew installed

Ensure you have cocoapods installed

* If not:

`brew install cocoapoads`

`pod install`

### How do I get set up? ###

* Open the .xcworkspace file

* Run Simulator

### How do I use the app? ###

* Open the app on an iPhone
* Click 'New Order'
* Select a Beverage
* Select up to 2 toppings
* Click 'Create Drink'
* Add an 'Order Name Reference'
* Either 'Save For Later' or 'Confirm Order'
* Returns to Homepage having submitted or saved your order
* Click Order History to see past orders
* Select any order to see its information 
* Click either 'Save For Later' or 'Confirm Order' to submit the order again
* Drinks, Toppings & Customers Confirmed Orders are all stored on Firebase

### How do I run Unit Tests? ###
* Select 'KekhaneTests' in the side bar
* Select the .swift file
* Click the small diamond in the margin next to 'class DrinksViewControllerTest: XCTestCase'
* This runs a test that verifies the logic which calculates an order total based on a CustomerOrder Object via mock object injection and view instantiation

### How do I run UI Tests? ###
* Select 'KekhaneUITests' in the side bar
* Select the .swift file
* Click the small diamond in the margin next to 'testPlaceOrder'
* This runs a text that verifies user touches and flow through the app to place an order testing alerts for too many toppings

### Test Case Examples ###

### Test Case 1 - Simple Order ###

* User selects 'New Order'
* User selects 'Tea'
* 'Mint' and 'Honey' are selected
* User 'Creates Drink'
* User enters name into 'Reference' field and clicks order
* Price is shown for drink and order total
* Process completed for a 'Tea with Mint & Honey'

### Test Case 2 - Complex Order ###
* User selects 'New Order'
* User selects 'Tea'
* 'Mint' is selected
* User 'Creates Drink'
* User clicks the '+' to add another drink
* User selects 'Cappuccino'
* User selects 'Caramel' and 'Chocolate'
* User trys to select 'Vanilla' but is disallowed by an alert
* User 'Creates Drink'
* User enters name into 'Reference' field and clicks 'Save for Later'
* Later in the day User clicks 'Order History' 
* User selects the order from earlier that day
* User clicks 'Confirm Order' 
* Process completed for a 'Tea with Mint & Cappuccino with Caramel & Chocolate'

### Who do I talk to? ###

* Repo owner: j.warris@ntlworld.com
